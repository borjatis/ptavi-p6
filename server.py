#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import sys
import simplertp
import secrets
import random

success = "200 OK"
structure = "400 Bad Request"
process = "405 Method Not Allowed"


class SIPHandler(socketserver.DatagramRequestHandler):

    @staticmethod
    def split_adrr(addr):
        correct, user, ip = True, '', ''
        if not addr.startswith('sip:'):
            correct = False
        components = addr[4:].split('@')
        if len(components) != 2:
            correct = False
        user, ip = components
        return correct, user, ip

    def handle(self):
        msg = self.rfile.read()
        client = self.client_address
        print(f"Recibido de {str(client)}:")
        print(f"{msg.decode('utf-8')}.")
        components = msg.decode('utf-8').split()
        if len(components) != 3:
            result = structure
        else:
            method, addr, proto = components
            if method not in ('INVITE', 'BYE', 'ACK'):
                result = process
            else:
                correct, user, ip = self.split_adrr(addr)
                if not correct:
                    result = structure
                else:
                    result = success
        if (result == success) and (method == 'ACK'):
            pass
        else:
            msg = f"SIP/2.0 {result}\r\n"
            self.wfile.write(msg.encode('utf-8'))
            print(f"Sent: {msg}.")
        #if method:
            #self.wfile.write(b"Viene el RTP\r\n")
            #BIT = secrets.randbelow(1)
            #ALEAT = random()
            #RTP_header = simplertp.RtpHeader()
            #RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=ALEAT)
            #audio = simplertp.RtpPayloadMp3(sys.argv[3])
            #simplertp.send_rtp_packet(RTP_header, audio, ip, port)


def parse_args():
    correct = True
    if len(sys.argv) != 4:
        correct = False
    else:
        ip, port, file = sys.argv[1:4]
        try:
            port = int(port)
        except ValueError:
            correct = False

    if correct == False:
        sys.exit("Usage: python3 server.py <IP> <port> <audio_file>")
    else:
        return ip, port, file

def main():
    ip, port, file = parse_args()
    with socketserver.UDPServer((ip, port), SIPHandler) as serv:
        print("Listening...")
        serv.serve_forever()


if __name__ == "__main__":
    main()
