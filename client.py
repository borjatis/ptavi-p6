#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socket
import sys


def parse_args():
    correct = True
    if len(sys.argv) != 3:
        correct = False
    else:
        if sys.argv[1] not in ('INVITE', 'BYE'):
            correct = False
        else:
            method = sys.argv[1]
            invited = sys.argv[2].split('@')
            if len(invited) != 2:
                correct = False
            else:
                user = invited[0]
                components = invited[1].split(':')
                if len(components) != 2:
                    correct = False
                else:
                    ip = components[0]
                    try:
                        port = int(components[1])
                    except ValueError:
                        correct = False

    if correct == False:
        sys.exit("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")
    else:
        return method, user, ip, port

def main():
    method, user, ip, port = parse_args()
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        request = f"{method} sip:{user}@{ip} SIP/2.0"
        my_socket.sendto(request.encode('utf-8'), (ip, port))
        response = my_socket.recv(2048).decode('utf-8')
        lines = response.splitlines()
        first = lines.pop(0)

        source = f"o={user}@gemeil.com {ip}"
        type = f"m=audio {port} RTP \n"
        ack = f"ACK sip:{user}@{ip} SIP/2.0"
        if first == "SIP/2.0 200 OK":
            if method == 'INVITE':
                print("Content-Type: application/sdp \nContent-Length:")
                print("\n v=0 \n", source, "\n s=misesion \n t=0 \n", type)
                print("Sending ACK:", ack)
                my_socket.sendto(ack.encode('utf-8'), (ip, port))
        else:
            print("Error")
        print("Recibido:")
        print(response + "\n v=0 \n", source, "\n s=misesion \n t=0 \n", type)

    print("Terminando programa...")


if __name__ == "__main__":
    main()
